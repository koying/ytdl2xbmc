#ifndef CONSTANTS_H
#define CONSTANTS_H

#define PYTHONPATH_SETTING "PythonDir"
#define RTMPDUMPPATH_SETTING "RtmpdumpDir"
#define VLCPATH_SETTING "VlcDir"
#define YTDLPATH_SETTING "YtdlDir"
#define DEFOUTPUTPATH_SETTING "DefoutDir"
#define LASTOUTDIR "LastOutdir"
#define AUTOCLOSE_SETTING "Autoclose"
#define YTDLURL_SETTING "YtdlUrl"
#define USE_YTDLAPI_SETTING "UseYtdlApi"

#endif // CONSTANTS_H
