#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>

namespace Ui {
  class SettingsDialog;
}

class SettingsDialog : public QDialog
{
  Q_OBJECT

public:
  explicit SettingsDialog(QWidget *parent = 0);
  ~SettingsDialog();

protected:
  void changeEvent(QEvent *e);

private slots:
  void on_btPythonDir_clicked();

  void on_btYtdlDir_clicked();

  void on_btDefoutDir_clicked();

  void on_buttonBox_accepted();

  void on_btRtmpdumpPath_clicked();

  void on_btVlcPath_clicked();

private:
  Ui::SettingsDialog *ui;
};

#endif // SETTINGSDIALOG_H
