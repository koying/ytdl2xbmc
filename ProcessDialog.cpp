#include "ProcessDialog.h"
#include "ui_ProcessDialog.h"

#include "Constants.h"

#include <QPushButton>
#include "qsettings.h"

ProcessDialog::ProcessDialog(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::ProcessDialog)
{
  ui->setupUi(this);

  ui->buttonBox->button(QDialogButtonBox::Cancel)->show();
  ui->buttonBox->button(QDialogButtonBox::Close)->hide();
}

ProcessDialog::~ProcessDialog()
{
  delete ui;
}

void ProcessDialog::setTitle(const QString &title)
{
  ui->label->setText(title);
}

void ProcessDialog::start(const QString& cmdline)
{
  connect(&process, SIGNAL(readyReadStandardOutput()), this, SLOT(on_readyReadStandardOutput()));
  connect(&process, SIGNAL(readyReadStandardError()), this, SLOT(on_readyReadStandardError()));
  connect(&process, SIGNAL(finished(int,QProcess::ExitStatus)), this, SLOT(on_finished(int,QProcess::ExitStatus)));

  show();
  process.start(cmdline);
}

void ProcessDialog::changeEvent(QEvent *e)
{
  QDialog::changeEvent(e);
  switch (e->type()) {
    case QEvent::LanguageChange:
      ui->retranslateUi(this);
      break;
    default:
      break;
  }
}

void ProcessDialog::on_buttonBox_rejected()
{
  if (process.state() != QProcess::NotRunning)
  {
    process.kill();
  }
  this->deleteLater();
  reject();
}

void ProcessDialog::on_readyReadStandardOutput()
{
  QByteArray out = process.readAllStandardOutput();
  out.replace("\r", "");
  out.replace("\n", "");
  ui->textBrowser->append(out);
}

void ProcessDialog::on_readyReadStandardError()
{
  QByteArray out = process.readAllStandardError();
  out.replace("\r", "");
  out.replace("\n", "");
  ui->textBrowser->append(out);
}

void ProcessDialog::on_finished(int exitCode, QProcess::ExitStatus exitStatus)
{
  QSettings set;
  bool autoclose = set.value(AUTOCLOSE_SETTING).toBool();

  if (autoclose && exitStatus == QProcess::NormalExit && exitCode == 0)
  {
    this->deleteLater();
    reject();
  }
  else
  {
    if (ui && ui->buttonBox)
    {
      ui->buttonBox->button(QDialogButtonBox::Cancel)->hide();
      ui->buttonBox->button(QDialogButtonBox::Close)->show();
    }
  }
}
