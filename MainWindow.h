#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QProcess>
#include <QClipboard>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>

#include "VideoModel.h"

namespace Ui {
  class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

protected:
  void changeEvent(QEvent *e);

  void loadSettings();
  void execYtdl(const QString& url);
  void execYtdlLocal(const QString& url);
  void execYtdlApi(const QString& url);
  void saveYtdl(const QString& outdir);
  void saveNfo(const VideoItem& curItem, const QString& fileName);

private slots:
  void on_tbGo_clicked();
  void on_actionSettings_triggered();
  void on_tableView_customContextMenuRequested(const QPoint &pos);
  void on_actionGet_triggered();
  void on_actionClear_triggered();
  void on_edUrl_returnPressed();
  void on_actionGet_to_triggered();

  void onClipChanged();

  void cancelProcess();

  void on_actionOpenVLC_triggered();

  void onDownloadFinished(QNetworkReply* reply);
  void OnApiReadFinished(QNetworkReply* reply);
  void writeThumb(QNetworkReply* reply);

private:
  Ui::MainWindow *ui;

  VideoModel* m_model;

  QString m_PythonDir;
  QString m_RtmpdumpDir;
  QString m_VlcDir;
  QString m_YtdlDir;
  QString m_DefoutDir;
  QString m_YtdlUrl;
  bool m_UseYtdlApi;

  QNetworkAccessManager * mNetMan;

  QProcess* m_ytdl;
  QClipboard* m_clipboard;
  QString m_lastclip;

  bool isHttpRedirect(QNetworkReply* reply);
};

#endif // MAINWINDOW_H
