#ifndef FORMATDELEGATE_H
#define FORMATDELEGATE_H

#include <QItemDelegate>

#include "VideoModel.h"

class QModelIndex;
class QWidget;
class QVariant;

class FormatDelegate : public QItemDelegate
{
Q_OBJECT
public:
  FormatDelegate(VideoModel* model, QObject *parent = 0);

  QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
  void setEditorData(QWidget *editor, const QModelIndex &index) const;
  void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;
  void updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const;

protected:
  VideoModel* m_model;
};

#endif // FORMATDELEGATE_H
