#include "SettingsDialog.h"
#include "ui_SettingsDialog.h"

#include "Constants.h"

#include <QSettings>
#include <QFileDialog>

SettingsDialog::SettingsDialog(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::SettingsDialog)
{
  ui->setupUi(this);

  QSettings set;
  ui->edPythonPath->setText(set.value(PYTHONPATH_SETTING).toString());
  ui->edRtmpdumpPath->setText(set.value(RTMPDUMPPATH_SETTING).toString());
  ui->edVlcPath->setText(set.value(VLCPATH_SETTING).toString());
  ui->edYtdlPath->setText(set.value(YTDLPATH_SETTING).toString());
  ui->edDefoutPath->setText(set.value(DEFOUTPUTPATH_SETTING).toString());
  ui->cbAutoclose->setChecked(set.value(AUTOCLOSE_SETTING, false).toBool());
  ui->edYtdlUrl->setText(set.value(YTDLURL_SETTING).toString());
  ui->cbUseYtdlApi->setChecked(set.value(USE_YTDLAPI_SETTING, false).toBool());
}

SettingsDialog::~SettingsDialog()
{
  delete ui;
}

void SettingsDialog::changeEvent(QEvent *e)
{
  QDialog::changeEvent(e);
  switch (e->type()) {
    case QEvent::LanguageChange:
      ui->retranslateUi(this);
      break;
    default:
      break;
  }
}

void SettingsDialog::on_btPythonDir_clicked()
{
  QString dir = QFileDialog::getExistingDirectory(this, tr("Select Directory"),
                                                  ui->edPythonPath->text(),
                                                  QFileDialog::ShowDirsOnly
                                                  | QFileDialog::DontResolveSymlinks);
  if (!dir.isEmpty())
    ui->edPythonPath->setText(dir);
}

void SettingsDialog::on_btYtdlDir_clicked()
{
  QString dir = QFileDialog::getExistingDirectory(this, tr("Select Directory"),
                                                  ui->edYtdlPath->text(),
                                                  QFileDialog::ShowDirsOnly
                                                  | QFileDialog::DontResolveSymlinks);
  if (!dir.isEmpty())
    ui->edYtdlPath->setText(dir);
}

void SettingsDialog::on_btDefoutDir_clicked()
{
  QString dir = QFileDialog::getExistingDirectory(this, tr("Select Directory"),
                                                  ui->edDefoutPath->text(),
                                                  QFileDialog::ShowDirsOnly
                                                  | QFileDialog::DontResolveSymlinks);
  if (!dir.isEmpty())
    ui->edDefoutPath->setText(dir);
}

void SettingsDialog::on_btRtmpdumpPath_clicked()
{
  QString dir = QFileDialog::getExistingDirectory(this, tr("Select Directory"),
                                                  ui->edRtmpdumpPath->text(),
                                                  QFileDialog::ShowDirsOnly
                                                  | QFileDialog::DontResolveSymlinks);
  if (!dir.isEmpty())
    ui->edRtmpdumpPath->setText(dir);
}

void SettingsDialog::on_btVlcPath_clicked()
{
  QString dir = QFileDialog::getExistingDirectory(this, tr("Select Directory"),
                                                  ui->edVlcPath->text(),
                                                  QFileDialog::ShowDirsOnly
                                                  | QFileDialog::DontResolveSymlinks);
  if (!dir.isEmpty())
    ui->edVlcPath->setText(dir);
}

void SettingsDialog::on_buttonBox_accepted()
{
  QSettings set;

  set.setValue(PYTHONPATH_SETTING, ui->edPythonPath->text());
  set.setValue(RTMPDUMPPATH_SETTING, ui->edRtmpdumpPath->text());
  set.setValue(VLCPATH_SETTING, ui->edVlcPath->text());
  set.setValue(YTDLPATH_SETTING, ui->edYtdlPath->text());
  set.setValue(DEFOUTPUTPATH_SETTING, ui->edDefoutPath->text());
  set.setValue(AUTOCLOSE_SETTING, ui->cbAutoclose->isChecked());
  set.setValue(YTDLURL_SETTING, ui->edYtdlUrl->text());
  set.setValue(USE_YTDLAPI_SETTING, ui->cbUseYtdlApi->isChecked());
}

