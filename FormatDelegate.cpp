#include "FormatDelegate.h"

#include <QComboBox>
#include <QWidget>
#include <QModelIndex>
#include <QApplication>
#include <QString>

#include "VideoModel.h"

FormatDelegate::FormatDelegate(VideoModel* model, QObject *parent)
  :QItemDelegate(parent)
  , m_model(model)
{
}

QWidget *FormatDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
  QComboBox* editor = new QComboBox(parent);
  VideoItem vitem = m_model->data(index, Qt::UserRole+1).value<VideoItem>();
  for(unsigned int i = 0; i < vitem.formats.size(); ++i)
  {
    editor->addItem(vitem.formats[i]);
  }
  return editor;
}

void FormatDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
  QComboBox *comboBox = static_cast<QComboBox*>(editor);
  int value = index.model()->data(index, Qt::EditRole).toInt();
  comboBox->setCurrentIndex(value);
}

void FormatDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
  QComboBox *comboBox = static_cast<QComboBox*>(editor);
  model->setData(index, comboBox->currentIndex(), Qt::EditRole);
}

void FormatDelegate::updateEditorGeometry(QWidget *editor, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
  editor->setGeometry(option.rect);
}

