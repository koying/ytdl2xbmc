#include "MainWindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);

  QCoreApplication::setApplicationName("ytdl2xbmc");
  QCoreApplication::setOrganizationName("SemperPax");
  QCoreApplication::setOrganizationDomain("semperpax.com");
  QCoreApplication::setApplicationVersion("1.0.0");

  MainWindow w;
  w.resize(800, 600);
  w.show();

  return a.exec();
}
