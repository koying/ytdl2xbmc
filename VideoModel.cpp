#include "VideoModel.h"

#include <QDebug>

VideoModel::VideoModel( QObject * parent )
  : QAbstractTableModel(parent)
{
}

QVariant VideoModel::headerData(int section, Qt::Orientation /*orientation*/, int role) const
{
  if (role != Qt::DisplayRole)
    return QVariant();

  switch (section) {
    case 0:
      return "Title";

    case 1:
      return "Description";

    case 2:
      return "Date";

    case 3:
      return "Thumbnail";

    case 4:
      return "Filename";

    case 5:
      return "Format";

    default:
      return QVariant();
  }
}

int VideoModel::rowCount(const QModelIndex &parent) const
{
  return m_data.count();
}

QVariant VideoModel::data(const QModelIndex &index, int role) const
{
  if (!index.isValid())
    return QVariant();

  if (role == Qt::UserRole+1) {
    QVariant ret;
    ret.setValue(m_data[index.row()]);
    return ret;
  }

  if (role != Qt::DisplayRole && role != Qt::EditRole)
    return QVariant();

  const VideoItem& item = m_data[index.row()];
  switch (index.column()) {
    case 0:
      return item.title;

    case 1:
      return item.description;

    case 2:
      return item.date;

    case 3:
      return item.thumbUrl;

    case 4:
      return item.filename;

    case 5:
      if (role == Qt::DisplayRole)
        return item.formats.at(item.formatIdx);
      else
        return item.formatIdx;

    default:
      return QVariant();
  }
}

bool VideoModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
  if (role != Qt::EditRole)
    return false;

  if (!index.isValid())
    return false;

  VideoItem& item = m_data[index.row()];
  switch (index.column()) {
    case 0:
      item.title = value.toString();
      return true;

    case 1:
      item.description = value.toString();
      return true;

    case 2:
      item.date = value.toString();
      return true;

    case 3:
      item.thumbUrl = value.toString();
      return true;

    case 4:
      return false;

    case 5:
      item.formatIdx = value.toInt();
      return true;

    default:
      return false;
  }
}

Qt::ItemFlags VideoModel::flags(const QModelIndex &index) const
{
  return (Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsEnabled);
}

bool VideoModel::insertRows(int row, int count, const QModelIndex &parent)
{
  if (parent.isValid())
    return false;

  if (row < 0 || row > m_data.count())
    return false;

  for (int i=0; i<count; ++i) {
    beginInsertRows(parent, row, row);
    m_data.insert(row, VideoItem());
    endInsertRows();
  }

  return true;
}

void VideoModel::addItem(VideoItem item)
{
  int row = m_data.count();

  beginInsertRows(QModelIndex(), row, row);
  m_data.insert(row, item);
  endInsertRows();
}

void VideoModel::clear()
{
  beginResetModel();
  m_data.clear();
  endResetModel();
}
