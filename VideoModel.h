#ifndef VIDEOMODEL_H
#define VIDEOMODEL_H

#include <QAbstractTableModel>
#include <QVariant>

class VideoItem
{
public:
    VideoItem()
      : formatIdx(0)
    {}
    ~VideoItem() {}
    VideoItem(const VideoItem& other)
        : title(other.title)
        , description(other.description)
        , date(other.date)
        , rating(other.rating)
        , extractor(other.extractor)
        , filename(other.filename)
        , thumbUrl(other.thumbUrl)
        , manifest_url(other.manifest_url)
        , rtmpPath(other.rtmpPath)
        , origUrl(other.origUrl)
        , formatIdx(other.formatIdx)
        , format_ids(other.format_ids)
        , format_urls(other.format_urls)
        , formats(other.formats)
    {}

    QString title;
    QString description;
    QString date;
    QString rating;
    QString extractor;
    QString filename;
    QString thumbUrl;
    QString manifest_url;
    QString rtmpPath;

    QString origUrl;
    int formatIdx;
    QStringList format_ids;
    QStringList format_urls;
    QStringList formats;
};

class VideoModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    VideoModel( QObject * parent = 0 );

    QVariant headerData ( int section, Qt::Orientation orientation, int role = Qt::DisplayRole ) const;
    int rowCount ( const QModelIndex & parent = QModelIndex() ) const;
    int columnCount ( const QModelIndex & parent = QModelIndex() ) const { return 6; }
    QVariant data ( const QModelIndex & index, int role = Qt::DisplayRole ) const;
    bool setData ( const QModelIndex & index, const QVariant & value, int role = Qt::EditRole );
    Qt::ItemFlags flags ( const QModelIndex & index ) const;

    bool insertRows ( int row, int count, const QModelIndex & parent = QModelIndex() );

    void addItem(VideoItem item);
    void clear();

private:
    QList<VideoItem> m_data;
};

Q_DECLARE_METATYPE ( VideoItem )

#endif // VIDEOMODEL_H
