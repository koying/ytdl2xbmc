#ifndef PROCESSDIALOG_H
#define PROCESSDIALOG_H

#include <QDialog>
#include <QProcess>

namespace Ui {
  class ProcessDialog;
}

class ProcessDialog : public QDialog
{
  Q_OBJECT

public:
  explicit ProcessDialog(QWidget *parent = 0);
  ~ProcessDialog();

  QProcess process;
  void setTitle(const QString& title);
  void start(const QString& cmdline);

protected:
  void changeEvent(QEvent *e);

private slots:
  void on_buttonBox_rejected();
  void on_readyReadStandardOutput();
  void on_readyReadStandardError();
  void on_finished(int exitCode, QProcess::ExitStatus exitStatus);

private:
  Ui::ProcessDialog *ui;
};

#endif // PROCESSDIALOG_H
