#include "MainWindow.h"
#include "ui_MainWindow.h"

#include "Constants.h"
#include "SettingsDialog.h"
#include "ProcessDialog.h"
#include "FormatDelegate.h"

#include <QtCore>
#include <QtGui>
#include <QtXml/QtXml>

#include <QMenu>
#include <QFileDialog>
#include <QProgressDialog>

#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>

MainWindow::MainWindow(QWidget *parent)
  : QMainWindow(parent)
  , ui(new Ui::MainWindow)
  , mNetMan(new QNetworkAccessManager(this))
  , m_ytdl(nullptr)
{
  ui->setupUi(this);

  m_model = new VideoModel(this);
  ui->tableView->setModel(m_model);
  ui->tableView->setContextMenuPolicy(Qt::CustomContextMenu);

  FormatDelegate* delegate = new FormatDelegate(m_model, this);
  ui->tableView->setItemDelegateForColumn(5, delegate);

  m_clipboard = QApplication::clipboard();
  connect(m_clipboard, SIGNAL(dataChanged()), this, SLOT(onClipChanged()));

  mNetMan->setRedirectPolicy(QNetworkRequest::NoLessSafeRedirectPolicy);
  connect(mNetMan, SIGNAL(finished(QNetworkReply*)),
          SLOT(onDownloadFinished(QNetworkReply*)));

  loadSettings();
}

MainWindow::~MainWindow()
{
  delete ui;
}

void MainWindow::changeEvent(QEvent *e)
{
  QMainWindow::changeEvent(e);
  switch (e->type()) {
    case QEvent::LanguageChange:
      ui->retranslateUi(this);
      break;
    default:
      break;
  }
}

void MainWindow::on_tbGo_clicked()
{
  execYtdl(ui->edUrl->text());
}

void MainWindow::on_edUrl_returnPressed()
{
  execYtdl(ui->edUrl->text());
}

void MainWindow::on_actionSettings_triggered()
{
  SettingsDialog* dlg = new SettingsDialog(this);
  if (dlg->exec() == QDialog::Accepted) {
    loadSettings();
  }
}

void MainWindow::on_tableView_customContextMenuRequested(const QPoint &pos)
{
  QModelIndex idx = ui->tableView->indexAt(pos);
  QMenu* ctxMenu = new QMenu(ui->tableView);

  if (idx.isValid())
  {
    ctxMenu->addAction(ui->actionGet);
    ctxMenu->addAction(ui->actionGet_to);
    ctxMenu->addSeparator();
    ctxMenu->addAction(ui->actionOpenVLC);
    ctxMenu->addAction(ui->actionGenerateNFO);
    ctxMenu->addSeparator();
  }
  ctxMenu->addAction(ui->actionClear);
  ctxMenu->popup(ui->tableView->viewport()->mapToGlobal(pos));
}

void MainWindow::loadSettings()
{
  QSettings set;
  m_PythonDir = set.value(PYTHONPATH_SETTING).toString();
  m_RtmpdumpDir = set.value(RTMPDUMPPATH_SETTING).toString();
  m_VlcDir = set.value(VLCPATH_SETTING).toString();
  m_YtdlDir = set.value(YTDLPATH_SETTING).toString();
  m_DefoutDir = set.value(DEFOUTPUTPATH_SETTING).toString();
  m_YtdlUrl = set.value(YTDLURL_SETTING).toString();
  m_UseYtdlApi = set.value(USE_YTDLAPI_SETTING).toBool();
}

void MainWindow::execYtdl(const QString& url)
{
  if (m_UseYtdlApi)
    execYtdlApi(url);
  else
    execYtdlLocal(url);
}

void MainWindow::execYtdlApi(const QString& url)
{
  QString apiuri = m_YtdlUrl + "?url=" + url;
  mNetMan->get(QNetworkRequest(apiuri));
}

void MainWindow::OnApiReadFinished(QNetworkReply* reply)
{
  QJsonParseError error;
  QJsonDocument jd = QJsonDocument::fromJson(reply->readAll(), &error);

  if (error.error != QJsonParseError::NoError)
  {
    qDebug() << "JSON parse error: " << error.errorString();
    ui->statusBar->showMessage("JSON parsing error: " + error.errorString());
    return;
  }

  QJsonObject aInfo = jd.object()["info"].toObject();
  if (aInfo.isEmpty())
    return;

  VideoItem* vitem = new VideoItem;
  vitem->title = aInfo["title"].toString();
  QDateTime dt = QDateTime::fromString(aInfo["upload_date"].toString().left(8), "yyyyMMdd");
  QString dtFilename;
  if (dt.isValid())
  {
    vitem->date = dt.toString("yyyy-MM-dd");
    dtFilename = dt.toString("yyyyMMdd");
  }
  else
  {
    vitem->date = QDateTime::currentDateTime().toString("yyyy-MM-dd");
    dtFilename = QDateTime::currentDateTime().toString("yyyyMMdd");
  }
  vitem->description = aInfo["description"].toString();
  vitem->thumbUrl = aInfo["thumbnail"].toString();
  vitem->extractor = aInfo["extractor"].toString();

  if (aInfo.contains("webpage_url"))
    vitem->origUrl = aInfo["webpage_url"].toString();
  else
    vitem->origUrl = jd.object()["url"].toString();

  QString filename = vitem->title + "-" + dtFilename + "-" + aInfo["id"].toString();
  filename.replace(' ', '.');
  filename.replace(QRegExp(QString::fromUtf8("[� âä]"), Qt::CaseInsensitive), "a");
  filename.replace(QRegExp(QString::fromUtf8("[éèêë]"), Qt::CaseInsensitive), "e");
  filename.replace(QRegExp(QString::fromUtf8("[ïî]"), Qt::CaseInsensitive), "i");
  filename.replace(QRegExp(QString::fromUtf8("[\u2019\"':,;\?]")), "_");
  filename.replace(QRegExp(QString::fromUtf8("[\u00b0]")), "");
  filename.replace(QRegExp("[/]"), "-");
  vitem->filename = filename;

  QJsonArray aRes = aInfo["formats"].toArray();
  for (int i=0; i<aRes.size(); ++i)
  {
    QJsonObject res = aRes[i].toObject();
    QString id = res["id"].toString();

    QString format_note = res["format_note"].toString();
    if (format_note.startsWith("ST") || format_note.contains("DASH") || res["ext"].toString() == "webm")
      continue;

    vitem->format_ids.append(res["format_id"].toString());
    vitem->format_urls.append(res["url"].toString());
    vitem->formats.append(res["format"].toString());

    int width = res["width"].toInt();
    if (width > 1000 && width < 1800)
    {
      vitem->formatIdx = vitem->formats.size()-1;
      vitem->manifest_url = res["manifest_url"].toString();
      vitem->rtmpPath = res["play_path"].toString();
    }
  }

  m_model->addItem(*vitem);
  delete vitem;

  ui->tableView->resizeColumnToContents(0);
  ui->tableView->resizeColumnToContents(2);
  ui->tableView->resizeColumnToContents(5);
}

bool MainWindow::isHttpRedirect(QNetworkReply *reply)
{
  int statusCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
  return statusCode == 301 || statusCode == 302 || statusCode == 303
      || statusCode == 305 || statusCode == 307 || statusCode == 308;
}

void MainWindow::writeThumb(QNetworkReply* reply)
{
  QString filename = reply->request().attribute(QNetworkRequest::User).toString();
  QImage img;
  if (img.loadFromData(reply->readAll()))
    img.save(filename);
  else
    qCritical() << "Invalid thumb:" << reply->url().toString();
}

void MainWindow::execYtdlLocal(const QString& url)
{
  m_ytdl = new QProcess(this);

#ifdef Q_OS_WIN
  QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
  env.insert("PATH", m_PythonDir+ ";" + env.value("PATH"));
  env.insert("PYTHONHOME", m_PythonDir);
  m_ytdl->setProcessEnvironment(env);

  QString python_exe = m_PythonDir + "\\python.exe";
#else
  QString python_exe = "python";
#endif

  QProgressDialog progress("Working...", "Cancel", 0, 0, this);
  progress.setRange(0, 0);
  connect(&progress, SIGNAL(canceled()), this, SLOT(cancelProcess()));
  progress.show();

  m_ytdl->setWorkingDirectory(m_YtdlDir);
  m_ytdl->start(python_exe + " __main__.py --all-formats -j \"" + url + "\"");
  if (!m_ytdl->waitForFinished(-1))
  {
    qDebug() << m_ytdl->errorString();
    m_ytdl->deleteLater();
    m_ytdl = NULL;
    return;
  }

  QByteArray result = m_ytdl->readAllStandardError();
  qDebug(result);

  int n;
  char buf[10000];
  m_ytdl->setReadChannel(QProcess::StandardOutput);
  result.clear();
  while ((n = m_ytdl->readLine(buf, 10000)) >= 0)
  {
    result += QString::fromUtf8(buf).simplified() + ",";
  }
  result.truncate(result.size()-1);
  QByteArray json = "{\"res\": [" + result + "]}";
  m_ytdl->deleteLater();
  m_ytdl = NULL;


  QFile dbg("d:/tmp.json");
  if (dbg.open(QIODevice::WriteOnly))
  {
    dbg.write(json);
    dbg.close();
  }

  QJsonParseError error;
  QJsonDocument jd = QJsonDocument::fromJson(json, &error);
  if (error.error != QJsonParseError::NoError)
  {
    qDebug() << "JSON parse error: " << error.errorString();
    ui->statusBar->showMessage("JSON parsing error: " + error.errorString());
    return;
  }

  QString curid = "";
  VideoItem* vitem = NULL;
  QJsonArray aRes = jd.object()["res"].toArray();
  for (int i=0; i<aRes.size(); ++i)
  {
    QJsonObject res = aRes[i].toObject();
    QString id = res["id"].toString();

    if (id != curid)
    {
      if (vitem)
      {
        m_model->addItem(*vitem);
        delete vitem;
      }

      vitem = new VideoItem;
      vitem->title = res["title"].toString();
      QDateTime dt = QDateTime::fromString(res["upload_date"].toString().left(8), "yyyyMMdd");
      if (dt.isValid())
        vitem->date = dt.toString("yyyy-MM-dd");
      else
        vitem->date = QDateTime::currentDateTime().toString("yyyy-MM-dd");
      vitem->description = res["description"].toString();
      vitem->thumbUrl = res["thumbnail"].toString();
      vitem->extractor = res["extractor"].toString();

      if (res.contains("webpage_url"))
        vitem->origUrl = res["webpage_url"].toString();
      else
        vitem->origUrl = url;
      QString filename = res["_filename"].toString();
      filename.replace(' ', '.');
      filename.replace(QRegExp(QString::fromUtf8("[� âä]"), Qt::CaseInsensitive), "a");
      filename.replace(QRegExp(QString::fromUtf8("[éèêë]"), Qt::CaseInsensitive), "e");
      filename.replace(QRegExp(QString::fromUtf8("[ïî]"), Qt::CaseInsensitive), "i");
      filename.replace(QRegExp(QString::fromUtf8("[\u2019\"':,;\?]")), "_");
      filename.replace(QRegExp(QString::fromUtf8("[\u00b0]")), "");
      filename.replace(QRegExp("[/]"), "-");
      vitem->filename = filename;

      curid = id;
    }
    QString format_note = res["format_note"].toString();
    if (format_note.startsWith("ST") || format_note.contains("DASH") || res["ext"].toString() == "webm")
      continue;

    vitem->format_ids.append(res["format_id"].toString());
    vitem->format_urls.append(res["url"].toString());
    vitem->formats.append(res["format"].toString());

    int width = res["width"].toInt();
    if (width > 1000 && width < 1800)
    {
      vitem->formatIdx = vitem->formats.size()-1;
      vitem->manifest_url = res["manifest_url"].toString();
      vitem->rtmpPath = res["play_path"].toString();
    }
  }
  if (vitem)
  {
    m_model->addItem(*vitem);
    delete vitem;
  }

  ui->tableView->resizeColumnToContents(0);
  ui->tableView->resizeColumnToContents(2);
  ui->tableView->resizeColumnToContents(4);
}

void MainWindow::saveYtdl(const QString& outdir)
{
  QModelIndexList selected = ui->tableView->selectionModel()->selectedRows();
  foreach(const QModelIndex &index, selected){
    VideoItem curItem = m_model->data(index, Qt::UserRole+1).value<VideoItem>();

    QString basename = curItem.filename;
    if (curItem.extractor.startsWith("arte", Qt::CaseInsensitive))
      basename += ".Arte7.S0E0";

    QString NfoFn = outdir + "/" + basename + ".nfo";
    saveNfo(curItem, NfoFn);

    QString thumbUrl = curItem.thumbUrl;
    QNetworkRequest req(thumbUrl);
    req.setAttribute(QNetworkRequest::User, outdir + "/" + basename + ".jpg");
    mNetMan->get(req);

    ProcessDialog* ytdl = new ProcessDialog(this);

#ifdef Q_OS_WIN
    QProcessEnvironment env = QProcessEnvironment::systemEnvironment();

    QString python_exe = "python.exe";
    if (!m_PythonDir.isEmpty())
    {
      env.insert("PATH", m_PythonDir+ ";" + env.value("PATH"));
      env.insert("PYTHONHOME", m_PythonDir);

      python_exe = m_PythonDir + "\\python.exe";
    }

    if (!m_RtmpdumpDir.isEmpty())
      env.insert("PATH", m_RtmpdumpDir + ";" + env.value("PATH"));

    ytdl->process.setProcessEnvironment(env);
#else
    QString python_exe = "python";
#endif

    ytdl->process.setWorkingDirectory(m_YtdlDir);
    QString cmdline = python_exe + " __main__.py --no-continue -f " + curItem.format_ids[curItem.formatIdx] + "+bestaudio/" + curItem.format_ids[curItem.formatIdx] + "/best -o \"" + outdir + QDir::separator() + basename + ".%(ext)s\"" + " \"" + curItem.origUrl + "\"";

    qDebug() << cmdline;
    ytdl->setTitle(basename);
    ytdl->start(cmdline);

  }
}

void MainWindow::saveNfo(const VideoItem& curItem, const QString& fileName)
{
  QDomDocument doc;
  QDomElement root = doc.createElement("episodedetails");
  doc.appendChild(root);

  QDomElement el = doc.createElement("title");
  root.appendChild(el);
  el.appendChild(doc.createTextNode(curItem.title));

  el = doc.createElement("plot");
  root.appendChild(el);
  el.appendChild(doc.createTextNode(curItem.description));

  el = doc.createElement("aired");
  root.appendChild(el);
  el.appendChild(doc.createTextNode(curItem.date));

  el = doc.createElement("thumb");
  root.appendChild(el);
  el.appendChild(doc.createTextNode(curItem.thumbUrl));

  el = doc.createElement("season");
  root.appendChild(el);
  el.appendChild(doc.createTextNode("0"));

  el = doc.createElement("episode");
  root.appendChild(el);
  el.appendChild(doc.createTextNode("0"));

  QFile nfo(fileName);
  if (!nfo.open(QIODevice::WriteOnly)) {
    ui->statusBar->showMessage("Impossible d'écrire le NFO");
    return;
  }
  nfo.write(doc.toString().toUtf8());
  nfo.close();
}

void MainWindow::on_actionGet_to_triggered()
{
  QSettings set;
  QString dir = QFileDialog::getExistingDirectory(this, tr("Select Directory"),
                                                  set.value(LASTOUTDIR, "").toString(),
                                                  QFileDialog::ShowDirsOnly
                                                  | QFileDialog::DontResolveSymlinks);
  if (!dir.isEmpty())
  {
    set.setValue(LASTOUTDIR, dir);

    saveYtdl(dir);
  }
}

void MainWindow::onClipChanged()
{
  QString clip = m_clipboard->text();
  if (clip == m_lastclip)
    return;

  m_lastclip = clip;
  QUrl url = QUrl::fromUserInput(clip);
  if (url.isValid() && !url.isLocalFile())
  {
    ui->edUrl->setText(clip);
    execYtdl(clip);
  }
}

void MainWindow::cancelProcess()
{
  if (!m_ytdl)
    return;

  m_ytdl->kill();
}

void MainWindow::on_actionGet_triggered()
{
  if (m_DefoutDir.isEmpty())
    return;

  saveYtdl(m_DefoutDir);
}


void MainWindow::on_actionClear_triggered()
{
  m_model->clear();
}

void MainWindow::on_actionOpenVLC_triggered()
{
  QModelIndexList selected = ui->tableView->selectionModel()->selectedRows();
  VideoItem curItem = m_model->data(selected.at(0), Qt::UserRole+1).value<VideoItem>();

  QString cmdline;
  if (!curItem.manifest_url.isEmpty())
    cmdline = m_VlcDir + "\\vlc.exe" + " \"" + curItem.manifest_url + "\"";
  else
    cmdline = m_VlcDir + "\\vlc.exe" + " \"" + curItem.format_urls[curItem.formatIdx] + "\"";


  qDebug() << cmdline;

  QProcess process;

  QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
  env.insert("PATH", m_PythonDir + ";" + m_RtmpdumpDir + ";" + env.value("PATH"));
  env.insert("PYTHONHOME", m_PythonDir);

  process.setProcessEnvironment(env);
  process.startDetached(cmdline);
}

void MainWindow::onDownloadFinished(QNetworkReply* reply)
{
  QUrl url = reply->url();
  if (reply->error())
  {
    qCritical("Download of %s failed: %s\n",
            url.toEncoded().constData(),
            qPrintable(reply->errorString()));
    return;
  }
  else if (isHttpRedirect(reply))
  {
    qInfo("Request was redirected: %s", url.toEncoded().constData());
    return;
  }
  else
  {
    if (reply->request().url().url().startsWith(m_YtdlUrl))
      OnApiReadFinished(reply);
    else if (!reply->request().attribute(QNetworkRequest::User).isNull())
      writeThumb(reply);
  }

}
