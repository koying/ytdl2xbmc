#-------------------------------------------------
#
# Project created by QtCreator 2014-05-16T12:52:49
#
#-------------------------------------------------

QT       += core gui network xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ytdl2xbmc
TEMPLATE = app


SOURCES += main.cpp\
        MainWindow.cpp \
    VideoModel.cpp \
    SettingsDialog.cpp \
    ProcessDialog.cpp \
    FormatDelegate.cpp

HEADERS  += MainWindow.h \
    VideoModel.h \
    SettingsDialog.h \
    Constants.h \
    ProcessDialog.h \
    FormatDelegate.h

FORMS    += MainWindow.ui \
    SettingsDialog.ui \
    ProcessDialog.ui

RC_FILE = ytdl2xbmc.rc

RESOURCES += \
    resource.qrc
